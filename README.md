# xringbuf

xringbuf (xan's ring buffer) is a simple ring buffer implementation for byte
streams. It's designed to be used in a SPSC (single producer, single consumer)
environment with either the producer or the consumer preëmpting the other (e.g.
with an interrupt handler, or across a pair of threads). xringbuf is
configurable, and supports locked and lock-free operation, stdatomic or
platforms lacking it, etc. It is optimized for fast operation and small
implementation size.

**WARNING:** xringbuf is *not* open source, though you may use it for yourself.
If you do so, please link to it (e.g. using a submodule reference) rather than
copying it. See [COPYING.md](COPYING.md) for more information.

There is a submodule in this repository, but you only need to pull it if you
intend to run the tests.

## Quick reference / usage summary

The documentation in [xringbuf.h](inc/xringbuf.h) takes precedence over this if
they go out of sync; this is copied in here for convenience so you can see what
this thing does.

### Types:

```
xringbuf_t................................... the buffer type
```

### Normal functions for main context

```
size_t xringbuf_init(xrb, buffer, len)....... initialize an xringbuf
bool   xringbuf_push(xrb, data, len)......... push all of data or nothing
size_t xringbuf_push_some(xrb, data, len).... push as much as possible
bool   xringbuf_push_one(xrb, data).......... push just one byte
bool   xringbuf_pop(xrb, data, len).......... pop len bytes or nothing
size_t xringbuf_pop_some(xrb, data, len)..... pop up to len bytes
bool   xringbuf_pop_one(xrb, data)........... pop just one byte
void   xringbuf_clear(xrb)................... dump everything out
size_t xringbuf_content(xrb)................. how much content is in xrb
size_t xringbuf_space(xrb)................... how much space is in xrb
bool   xringbuf_empty(xrb)................... is xrb empty
```

### Non-locking functions for interrupt/signal context

```
bool   xringbuf_push_i(xrb, data, len)....... push all of data or nothing
size_t xringbuf_push_some_i(xrb, data, len).. push as much as possible
bool   xringbuf_push_one_i(xrb, data)........ push just one byte
bool   xringbuf_pop_i(xrb, data, len)........ pop len bytes or nothing
size_t xringbuf_pop_some_i(xrb, data, len)... pop up to len bytes
bool   xringbuf_pop_one_i(xrb, data)......... pop just one byte
void   xringbuf_clear_i(xrb)................. dump everything out
size_t xringbuf_content_i(xrb)............... how much content is in xrb
size_t xringbuf_space_i(xrb)................. how much space is in xrb
bool   xringbuf_empty_i(xrb)................. is xrb empty
```

In a lockfree build, the `xringbuf_*()` and `xringbuf_*_i()` functions are
identical. If it's not too inconvenient, I suggest you still use the correct
set in each context to allow you to change the implementation later. In a
locking build, the `xringbuf_*_i()` functions skip locking, so they are safe
to use from a context that has preëmpted the main context and cannot safely
execute blocking lock functions.
