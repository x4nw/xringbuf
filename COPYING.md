# Anti-Open License v0 Draft

© 2024 by xan.

This project isn't "open source"! Open means open for everybody: cute gay
people, boring tech boys, defense contractors and Elon Musk. Those aren't all
company I want to keep. Nor do I believe true freedom is achieved by delivering
the most personal "freedoms" to the most individuals, including the Musks and
Bezoses and other people who hurt people with them.

You can use this for yourself and your friends without my permission, because
of course you can. But you cannot copy and distribute modified or unmodified
versions of this without asking first. Please do ask! If you aren't trying to
do anything horrible with my work, I'll probably say yes.

If you use it and you like it, consider sending me money for a coffee — and
then find someone in your life who needs that more than I do, and give it to
them instead. I'll be fine on kind words alone.
