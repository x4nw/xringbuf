// © by xan. All rights reserved. See accompanying COPYING.md for more info.

#include "xringbuf.h"

#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>

#ifndef XRB_STATIC_ALWAYS_INLINE
#  define XRB_STATIC_ALWAYS_INLINE static inline __attribute__((always_inline))
#endif

// Quick examples of buffer use to illustrate how we're using indices:
//
//                       ____ ____ ____ ____
// on init:        buf: |    |    |    |    | mask - (W-R)&mask = 3
//                        RW                         (W-R)&mask = 0
//
//                       ____ ____ ____ ____
// push 1:         buf: | 1  |    |    |    | mask - (W-R)&mask = 2
//                        R     W                    (W-R)&mask = 1
//
//                       ____ ____ ____ ____
// push 2:         buf: | 1  | 2  |    |    | mask - (W-R)&mask = 1
//                        R          W               (W-R)&mask = 2
//
//                       ____ ____ ____ ____
// push 3:         buf: | 1  | 2  | 3  |    | mask - (W-R)&mask = 0
//                        R               W          (W-R)&mask = 3
//
//                       ____ ____ ____ ____
// pop 1:          buf: |    | 2  | 3  |    | mask - (W-R)&mask = 1
//                             R          W          (W-R)&mask = 2
//
//                       ____ ____ ____ ____
// push 4:         buf: |    | 2  | 3  | 4  | mask - (W-R)&mask = 0
//                         W   R                     (W-R)&mask = 3

// xringbuf_content() and xringbuf_space() already exist, but to use these
// in push and pop functions i want careful control over where the rdi and
// wri reads take place. These macros take rdi and wri as values.

// Compute the amount of content currently in the buffer
#define CONTENT(xrb_, rdi_, wri_) XRINGBUF_WRAP((xrb_), (wri_) - (rdi_))

// Compute the available space for new elements.
#if XRINGBUF_ONLY_POW2
#define SPACE(xrb_, rdi_, wri_) ((xrb_)->mask - CONTENT((xrb_), (rdi_), (wri_)))
#else
#define SPACE(xrb_, rdi_, wri_) ((xrb_)->len - 1 - CONTENT((xrb_), (rdi_), (wri_)))
#endif

#define LOAD_RDI_WRI(xrb_, is_i_) \
	if (!(is_i_)) {XRINGBUF_LOCK;} \
	XRINGBUF_SYNC; \
	xringbuf_index_t rdi = (xrb_)->rdi; \
	xringbuf_index_t wri = (xrb_)->wri; \
	if (!(is_i_)) {XRINGBUF_UNLOCK;}

#define STORE_RDI(xrb_, is_i_, rdi_) \
	if (!(is_i)) {XRINGBUF_LOCK;} \
	(xrb_)->rdi = (rdi_); \
	if (!(is_i)) {XRINGBUF_UNLOCK;}

#define STORE_WRI(xrb_, is_i_, wri_) \
	if (!(is_i)) {XRINGBUF_LOCK;} \
	XRINGBUF_SYNC; \
	(xrb_)->wri = (wri_); \
	if (!(is_i)) {XRINGBUF_UNLOCK;}

size_t
xringbuf_init(xringbuf_t * xrb, void * buffer, size_t len)
{
	size_t count;

#if XRINGBUF_ONLY_POW2
	xringbuf_index_t mask = 0;
	while (len / 2)
	{
		len /= 2;
		mask = (mask << 1) | 1;
	}
	xrb->mask = mask;
	count = mask;
#else
	xringbuf_index_t const max = (xringbuf_index_t) -1;
	if (len && len - 1 > max)
		len = (size_t) max + 1;
	xrb->len = len;
	count = len ? len - 1 : 0;
#endif

	xrb->rdi = 0;
	xrb->wri = 0;
	xrb->buf = buffer;

	return count;
}

XRB_STATIC_ALWAYS_INLINE bool
_push_iq(xringbuf_t * xrb, void const * data, size_t len, bool is_i)
{
	char volatile * xrb_c = (char volatile *) xrb->buf;
	char const * data_c = (char const *) data;

	LOAD_RDI_WRI(xrb, is_i);

	if (SPACE(xrb, rdi, wri) < len)
		return false;

	for (size_t n = 0; n < len; n++, wri = XRINGBUF_WRAP(xrb, wri + 1))
	{
		xrb_c[wri] = data_c[n];
	}

	STORE_WRI(xrb, is_i, wri);

	return true;
}

XRB_STATIC_ALWAYS_INLINE size_t
_push_some_iq(xringbuf_t * xrb, void const * data, size_t len, bool is_i)
{
	char volatile * xrb_c = (char volatile *) xrb->buf;
	char const * data_c = (char const *) data;

	LOAD_RDI_WRI(xrb, is_i);

	size_t space = SPACE(xrb, rdi, wri);
	if (space < len)
		len = space;

	for (size_t n = 0; n < len; n++, wri = XRINGBUF_WRAP(xrb, wri + 1))
	{
		xrb_c[wri] = data_c[n];
	}

	STORE_WRI(xrb, is_i, wri);

	return len;
}

XRB_STATIC_ALWAYS_INLINE bool
_push_one_iq(xringbuf_t * xrb, void const * data, bool is_i)
{
	LOAD_RDI_WRI(xrb, is_i);

	xringbuf_index_t next_wri = XRINGBUF_WRAP(xrb, wri + 1);

	if (rdi == next_wri)
		return false;

	((char volatile *)xrb->buf)[wri] = *(char const *) data;

	STORE_WRI(xrb, is_i, next_wri);

	return true;
}

XRB_STATIC_ALWAYS_INLINE bool
_pop_iq(xringbuf_t * xrb, void * data, size_t len, bool is_i)
{
	char const volatile * xrb_c = (char const volatile *) xrb->buf;
	char * data_c = (char *) data;

	LOAD_RDI_WRI(xrb, is_i);

	if (CONTENT(xrb, rdi, wri) < len)
		return false;

	for (size_t i = 0; i < len; i++, rdi = XRINGBUF_WRAP(xrb, rdi + 1))
	{
		data_c[i] = xrb_c[rdi];
	}

	STORE_RDI(xrb, is_i, rdi);

	return true;
}

XRB_STATIC_ALWAYS_INLINE size_t
_pop_some_iq(xringbuf_t * xrb, void * data, size_t len, bool is_i)
{
	char const volatile * xrb_c = (char const volatile *) xrb->buf;
	char * data_c = (char *) data;

	LOAD_RDI_WRI(xrb, is_i)

	size_t content = CONTENT(xrb, rdi, wri);
	if (content < len)
		len = content;

	for (size_t i = 0; i < len; i++, rdi = XRINGBUF_WRAP(xrb, rdi + 1))
	{
		data_c[i] = xrb_c[rdi];
	}

	STORE_RDI(xrb, is_i, rdi);

	return len;
}

XRB_STATIC_ALWAYS_INLINE bool
_pop_one_iq(xringbuf_t * xrb, void * data, bool is_i)
{
	LOAD_RDI_WRI(xrb, is_i);

	if (rdi == wri)
		return false;

	*(char *) data = ((char const volatile *)xrb->buf)[rdi];

	rdi = XRINGBUF_WRAP(xrb, rdi + 1);

	STORE_RDI(xrb, is_i, rdi);

	return true;
}

XRB_STATIC_ALWAYS_INLINE void
_clear_iq(xringbuf_t * xrb, bool is_i)
{
	// In non-locking mode, the "race" condition that can occur here is
	// safe: it'll result in the buffer being not quite cleared. It's a
	// perfectly valid outcome, because these two sequences are equivalent:
	//
	// RACE HITS:
	//   xringbuf_clear()
	//     read wri
	//                        [someone stores something len=foo]
	//     write rdi
	//                        [now we have count=foo]
	//
	// RACE MISSES:
	//   xringbuf_clear()
	//     read wri
	//     write rdi
	//                        [someone stores something len=foo]
	//                        [now we have count=foo]

	if (!is_i) {XRINGBUF_LOCK;}
	XRINGBUF_SYNC;
	xringbuf_index_t wri = xrb->wri;
	xrb->rdi = wri;
	XRINGBUF_SYNC;
	if (!is_i) {XRINGBUF_UNLOCK;}
}

bool
xringbuf_push(xringbuf_t * xrb, void const * data, size_t len)
{ return _push_iq(xrb, data, len, false); }

size_t
xringbuf_push_some(xringbuf_t * xrb, void const * data, size_t len)
{ return _push_some_iq(xrb, data, len, false); }

bool
xringbuf_push_one(xringbuf_t * xrb, void const * data)
{ return _push_one_iq(xrb, data, false); }

bool
xringbuf_pop(xringbuf_t * xrb, void * data, size_t len)
{ return _pop_iq(xrb, data, len, false); }

size_t
xringbuf_pop_some(xringbuf_t * xrb, void * data, size_t len)
{ return _pop_some_iq(xrb, data, len, false); }

bool
xringbuf_pop_one(xringbuf_t * xrb, void * data)
{ return _pop_one_iq(xrb, data, false); }

void
xringbuf_clear(xringbuf_t * xrb)
{ _clear_iq(xrb, false); }

#if XRINGBUF_USE_LOCK

bool
xringbuf_push_i(xringbuf_t * xrb, void const * data, size_t len)
{ return _push_iq(xrb, data, len, true); }

size_t
xringbuf_push_some_i(xringbuf_t * xrb, void const * data, size_t len)
{ return _push_some_iq(xrb, data, len, true); }

bool
xringbuf_push_one_i(xringbuf_t * xrb, void const * data)
{ return _push_one_iq(xrb, data, true); }

bool
xringbuf_pop_i(xringbuf_t * xrb, void * data, size_t len)
{ return _pop_iq(xrb, data, len, true); }

size_t
xringbuf_pop_some_i(xringbuf_t * xrb, void * data, size_t len)
{ return _pop_some_iq(xrb, data, len, true); }

bool
xringbuf_pop_one_i(xringbuf_t * xrb, void * data)
{ return _pop_one_iq(xrb, data, true); }

void
xringbuf_clear_iq(xringbuf_t * xrb)
{ _clear_iq(xrb, true); }

#endif
