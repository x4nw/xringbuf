// xringbuf (xan's ring buffer)
// © by xan. All rights reserved. See accompanying COPYING.md for more info.
//
// xringbuf is a simple ring buffer implementation for byte streams. It's
// designed to be used in a SPSC (single producer, single consumer) environment
// with either the producer or the consumer preëmpting the other (e.g. with an
// interrupt handler, or across a pair of threads). xringbuf is configurable,
// and supports locked and lock-free operation, stdatomic or platforms lacking
// it, etc. It is optimized for fast operation and small implementation size.
//
// +--------------------------------------------------------------------------+
// | Usage summary                                                            |
// +--------------------------------------------------------------------------+
// The full documentation for every function is with its declaration. In
// general:
//
// TYPES:
// xringbuf_t................................... the buffer type
//
// NORMAL FUNCTIONS FOR MAIN CONTEXT:
// size_t xringbuf_init(xrb, buffer, len)....... initialize an xringbuf
// bool   xringbuf_push(xrb, data, len)......... push all of data or nothing
// size_t xringbuf_push_some(xrb, data, len).... push as much as possible
// bool   xringbuf_push_one(xrb, data).......... push just one byte
// bool   xringbuf_pop(xrb, data, len).......... pop len bytes or nothing
// size_t xringbuf_pop_some(xrb, data, len)..... pop up to len bytes
// bool   xringbuf_pop_one(xrb, data)........... pop just one byte
// void   xringbuf_clear(xrb)................... dump everything out
// size_t xringbuf_content(xrb)................. how much content is in xrb
// size_t xringbuf_space(xrb)................... how much space is in xrb
// bool   xringbuf_empty(xrb)................... is xrb empty
//
// NON-LOCKING FUNCTIONS FOR INTERRUPT/SIGNAL CONTEXT:
// bool   xringbuf_push_i(xrb, data, len)....... push all of data or nothing
// size_t xringbuf_push_some_i(xrb, data, len).. push as much as possible
// bool   xringbuf_push_one_i(xrb, data)........ push just one byte
// bool   xringbuf_pop_i(xrb, data, len)........ pop len bytes or nothing
// size_t xringbuf_pop_some_i(xrb, data, len)... pop up to len bytes
// bool   xringbuf_pop_one_i(xrb, data)......... pop just one byte
// void   xringbuf_clear_i(xrb)................. dump everything out
// size_t xringbuf_content_i(xrb)............... how much content is in xrb
// size_t xringbuf_space_i(xrb)................. how much space is in xrb
// bool   xringbuf_empty_i(xrb)................. is xrb empty
//
// In a lockfree build, the xringbuf_*() and xringbuf_*_i() functions are
// identical. If it's not too inconvenient, I suggest you still use the correct
// set in each context to allow you to change the implementation later. In a
// locking build, the xringbuf_*_i() functions skip locking, so they are safe
// to use from a context that has preëmpted the main context and cannot safely
// execute blocking lock functions.
//
// +--------------------------------------------------------------------------+
// | Configuring the build                                                    |
// +--------------------------------------------------------------------------+
// To use, you should define the following constants both when building
// xringbuf and when using it:
//
//   - XRINGBUF_INDEX_T...... is the type used for buffer indices. If you're
//       using the lockfree implementation, it must be a type that can be
//       accessed atomically.
//   - XRINGBUF_USE_LOCK..... is 1 to use locking or 0 to be lock-free.
//   - XRINGBUF_EXTRA_INCLUDE is an include directive to pull in anything
//       needed to access types and locking functions, for example
//       <stdatomic.h>
//   - XRINGBUF_LOCK......... is a statement to globally lock buffers
//   - XRINGBUF_UNLOCK....... is a statement to globally unlock buffers
//   - XRINGBUF_SYNC......... is a statement to synchronize memory access
//   - XRINGBUF_ONLY_POW2.... is 1 to only use power of two buffer sizes,
//       avoiding the modulus operation on platforms where that's slow (highly
//       recommended on anything without a fast divider!)
//
// Incorporating xringbuf into your project is simple (just define these,
// build xringbuf.c, and make this header accessible). A meson.build file is
// provided in case you want to use it from Meson, and this provides options
// to define these.
//
// If you do not define XRINGBUF_LOCK and XRINGBUF_UNLOCK and you use locking,
// they default to xringbuf_lock() and xringbuf_unlock(), which you must then
// provide.
//
// The Meson script includes some preset profiles that you may select with
// the xringbuf_profile option. These are:
//
//   - atomic:
//       XRINGBUF_INDEX_T       = atomic_uint
//       XRINGBUF_USE_LOCK      = 0
//       XRINGBUF_SYNC          = atomic_signal_fence(memory_order_seq_cst)
//       XRINGBUF_EXTRA_INCLUDE = <stdatomic.h>
//
//   - uintptr_unlocked:
//       XRINGBUF_INDEX_T       = uintptr_t
//       XRINGBUF_USE_LOCK      = 0
//       XRINGBUF_SYNC          = __sync_synchronize()
//
//   - avr_8bit_unlocked:
//       XRINGBUF_INDEX_T       = uint8_t
//       XRINGBUF_USE_LOCK      = 0
//       XRINGBUF_SYNC          = __sync_synchronize()
//
//   - avr_16bit_unlocked:
//       XRINGBUF_INDEX_T       = uint16_t
//       XRINGBUF_USE_LOCK      = 1
//       XRINGBUF_LOCK          = cli()
//       XRINGBUF_UNLOCK        = sei()
//       XRINGBUF_SYNC          = __sync_synchronize()
//       XRINGBUF_EXTRA_INCLUDE = <avr/interrupt.h>

#ifndef XRINGBUF_H
#define XRINGBUF_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>

#ifdef XRINGBUF_EXTRA_INCLUDE
# include XRINGBUF_EXTRA_INCLUDE
#endif

typedef XRINGBUF_INDEX_T xringbuf_index_t;

typedef struct xringbuf_s
{
	volatile xringbuf_index_t rdi;
	volatile xringbuf_index_t wri;
#if XRINGBUF_ONLY_POW2
	xringbuf_index_t mask;
#else
	// Actual length of the buffer, not the number of elements it can hold
	size_t len;
#endif
	void * buf;
} xringbuf_t;

#if XRINGBUF_USE_LOCK
# ifndef XRINGBUF_LOCK
#  define XRINGBUF_LOCK xringbuf_lock()
   extern void xringbuf_lock(void);
# endif
# ifndef XRINGBUF_UNLOCK
#  define XRINGBUF_UNLOCK xringbuf_unlock()
   extern void xringbuf_unlock(void);
# endif
#else
# ifndef XRINGBUF_LOCK
#  define XRINGBUF_LOCK
# endif
# ifndef XRINGBUF_UNLOCK
#  define XRINGBUF_UNLOCK
# endif
#endif

// Initialize an xringbuf.
//
// xrb:    xringbuf_t to initialize
// buffer: buffer to store data
// len:    length of the buffer.
//
// Returns the actual number of bytes the buffer will be able to store.
// If XRINGBUF_ONLY_POW2 is nonzero, this will always be one less than a power
// of two (if len is not a power of two, some buffer will go unused).
//
// If len is less than 2, the buffer capacity will be zero and the xringbuf
// will be unusable.
size_t xringbuf_init(
	xringbuf_t * xrb,
	void       * buffer,
	size_t       len
);

// Push to an xringbuf. The data will be pushed in a single, atomic operation;
// if there is no space in the buffer, returns immediately.
//
// xrb:  xringbuf_t to operate on
// data: data to push
// len:  length of data
//
// If len == 0, then data is allowed to be null.
//
// Returns true iff the data was pushed. A no-op when len == 0 counts as a
// successful push.
bool xringbuf_push(
	xringbuf_t * xrb,
	void const * data,
	size_t       len
);

// Push as many bytes as possible to an xringbuf. They will be pushed in a
// single, atomic operation.
//
// xrb:  xringbuf_t to operate on
// data: data to push
// len:  length of data
//
// If len == 0, then data is allowed to be null.
//
// Returns the number of bytes pushed.
size_t xringbuf_push_some(
	xringbuf_t * xrb,
	void const * data,
	size_t       len
);

// Quickly push a single byte to an xringbuf. This operation is faster and
// smaller than passing a length of 1 to the other functions.
//
// xrb:  xringbuf_t to operate on
// data: data (of length 1) to push
//
// Returns whether anything was pushed
bool xringbuf_push_one(
	xringbuf_t * xrb,
	void const * data
);

// Pop from an xringbuf. The data will be popped in a single, atomic operation.
// If the number of bytes requested do not exist in the buffer, returns
// immediately.
//
// xrb:  xringbuf_t to operate on
// data: buffer to hold popped data
// len:  length of data
//
// Returns true iff the data was popped. A no-op when len == 0 counts as a
// successful pop.
bool xringbuf_pop(
	xringbuf_t * xrb,
	void       * data,
	size_t       len
);

// Pop as many bytes as possible from an xringbuf. The data will be popped in a
// single, atomic operation.
//
// xrb:  xringbuf_t to operate on
// data: buffer to hold popped data
// len:  length of buf
//
// Returns the number of bytes popped.
size_t xringbuf_pop_some(
	xringbuf_t * xrb,
	void       * data,
	size_t       len
);

// Quickly pop a single byte from an xringbuf. This operation is faster and
// smaller than passing a length of 1 to the other functions.
//
// xrb:  xringbuf_t to operate on
// data: buffer (of length 1) to hold popped data
//
// Returns whether anything was popped.
bool xringbuf_pop_one(
	xringbuf_t * xrb,
	void       * data
);

// Pop everything into the void.
//
// xrb:  xringbuf_t to operate on
void xringbuf_clear(
	xringbuf_t * xrb
);

#define xringbuf__locked(xrb, ty, expr) \
	__extension__ \
	({ty _rsp; \
	   XRINGBUF_LOCK; \
	   _rsp = (expr); \
	   XRINGBUF_UNLOCK; _rsp;})

#if XRINGBUF_ONLY_POW2
#  define XRINGBUF_WRAP(xrb_, len_) ((len_) & (xrb_)->mask)
#else
#  define XRINGBUF_WRAP(xrb_, len_) ((len_) % ((xrb_)->len))
#endif

// Return the number of bytes of data currently in the buffer.
#define xringbuf_content_i(xrb_) \
	((size_t) XRINGBUF_WRAP(xrb_, (xrb_)->wri - (xrb_)->rdi))

// Return the number of bytes of space currently available.
#if XRINGBUF_ONLY_POW2
#  define xringbuf_space_i(xrb_) \
	((size_t)((xrb_)->mask - xringbuf_content((xrb_))))
#else
#  define xringbuf_space_i(xrb_) \
	((size_t)((xrb_)->len - 1 - xringbuf_content((xrb_))))
#endif

// Check if the buffer is empty
#define xringbuf_empty_i(xrb_) \
	((xrb_)->rdi == (xrb_)->wri)

#if XRINGBUF_USE_LOCK
#define xringbuf_content(xrb) xringbuf__locked(xrb, size_t, xringbuf_content_i((xrb)))
#define xringbuf_space(xrb)   xringbuf__locked(xrb, size_t, xringbuf_space_i((xrb)))
#define xringbuf_empty(xrb)   xringbuf__locked(xrb, size_t, xringbuf_empty_i((xrb)))

bool xringbuf_push_i(xringbuf_t * xrb, void const * data, size_t len);
size_t xringbuf_push_some_i(xringbuf_t * xrb, void const * data, size_t len);
bool xringbuf_push_one_i(xringbuf_t * xrb, void const * data);
bool xringbuf_pop_i(xringbuf_t * xrb, void * data, size_t len);
size_t xringbuf_pop_some_i(xringbuf_t * xrb, void * data, size_t len);
bool xringbuf_pop_one_i(xringbuf_t * xrb, void * data);
bool xringbuf_clear_i(xringbuf_t * xrb);

#else
#define xringbuf_content(xrb) xringbuf_content_i(xrb)
#define xringbuf_space(xrb)   xringbuf_space_i(xrb)
#define xringbuf_empty(xrb)   xringbuf_empty_i(xrb)
#define xringbuf_push_i       xringbuf_push
#define xringbuf_push_some_i  xringbuf_push_some
#define xringbuf_push_one_i   xringbuf_push_one
#define xringbuf_pop_i        xringbuf_pop
#define xringbuf_pop_some_i   xringbuf_pop_some
#define xringbuf_pop_one_i    xringbuf_pop_one
#define xringbuf_clear_i      xringbuf_clear
#endif

#ifdef __cplusplus
}
#endif

#endif // !defined(XRINGBUF_H)
