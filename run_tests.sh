#!/bin/bash

set -e
TEST_DIR=build_test

function run_test() {
	echo "================================================================"
	echo "Using options: " "$@"
	echo "----------------------------------------------------------------"
	rm -rf $TEST_DIR
	meson setup $TEST_DIR "$@"
	meson compile -C $TEST_DIR
	meson test -C $TEST_DIR
}

run_test -D xringbuf_profile=atomic -D xringbuf_only_pow2=false
run_test -D xringbuf_profile=uintptr_unlocked -D xringbuf_only_pow2=false
run_test -D xringbuf_profile=atomic -D xringbuf_only_pow2=true
run_test -D xringbuf_profile=uintptr_unlocked -D xringbuf_only_pow2=true
rm -rf $TEST_DIR
