// © by xan. All rights reserved. See accompanying COPYING.md for more info.

#include "xringbuf.h"

#include <stdio.h>
#include <string.h>
#include <assert.h>

#include <tap.h>

uint8_t BUFFER[65536];

#if XRINGBUF_ONLY_POW2
#  define cmpbuf_ok(a, b, ...) \
	ok (((a)->rdi == (b)->rdi) && \
	    ((a)->wri == (b)->wri) && \
	    ((a)->mask == (b)->mask) && \
	    ((a)->buf == (b)->buf), ##__VA_ARGS__)
#else
#  define cmpbuf_ok(a, b, ...) \
	ok (((a)->rdi == (b)->rdi) && \
	    ((a)->wri == (b)->wri) && \
	    ((a)->len == (b)->len) && \
	    ((a)->buf == (b)->buf), ##__VA_ARGS__)
#endif

int net_locks = 0;
int total_locks = 0;
int total_unlocks = 0;

void
xringbuf_lock(void)
{
	net_locks++;
	total_locks++;
}

void
xringbuf_unlock(void)
{
	net_locks--;
	total_unlocks++;
}

static void
reset_locks(void)
{
	net_locks = 0;
	total_locks = 0;
	total_unlocks = 0;
}

// We run the lock check a LOT - it's quiet unless it fails to avoid making
// a whole pile of test pass outputs.
#define NO_LOCK_COUNT -1
#define SOME_LOCKS -2
static void
check_reset_locks(int expected_locks)
{
	bool cond = false;
#if XRINGBUF_USE_LOCK
	if (expected_locks >= 0)
		cond = (net_locks == 0 && total_locks == expected_locks);
	else if (expected_locks == NO_LOCK_COUNT)
		cond = (net_locks == 0);
	else if (expected_locks == SOME_LOCKS)
		cond = (net_locks == 0 && total_locks > 0);
#else
	(void) expected_locks;
	cond = true;
#endif
	if (!cond)
		fail("lock check");
	reset_locks();
}

int
main(int argc, char ** argv)
{
	xringbuf_t xrb;
	size_t size, count;
	bool success;
	char tempbuf[256];

	(void) argc;
	(void) argv;

	plan(NO_PLAN); // hashtag yolo motherfuckerssss

	size = xringbuf_init(&xrb, BUFFER, 0);
	cmp_ok(size, "==", 0, "init with len=0");

	size = xringbuf_init(&xrb, BUFFER, 1);
	cmp_ok(size, "==", 0, "init with len=1");

	size = xringbuf_init(&xrb, BUFFER, 65536);
	cmp_ok(size, "==", 65536-1, "init with power-of-two len");
	cmp_ok(xringbuf_space(&xrb), "==", size, "xringbuf_space");
	cmp_ok(xringbuf_content(&xrb), "==", 0, "xringbuf_content");
	ok(xringbuf_empty(&xrb), "xringbuf_empty");
	check_reset_locks(SOME_LOCKS);

	size = xringbuf_init(&xrb, BUFFER, 60000);
#if XRINGBUF_ONLY_POW2
	cmp_ok(size, "==", 32767, "init with non power-of-two len");
#else
	cmp_ok(size, "==", 59999, "init with non power-of-two len");
#endif
	cmp_ok(xringbuf_space(&xrb), "==", size, "xringbuf_space");
	cmp_ok(xringbuf_content(&xrb), "==", 0, "xringbuf_content");
	ok(xringbuf_empty(&xrb), "xringbuf_empty");
	check_reset_locks(SOME_LOCKS);

	size = xringbuf_init(&xrb, BUFFER, 8);
	cmp_ok(size, "==", 7, "init with small len");
	cmp_ok(xringbuf_space(&xrb), "==", size, "xringbuf_space");
	cmp_ok(xringbuf_content(&xrb), "==", 0, "xringbuf_content");
	ok(xringbuf_empty(&xrb), "xringbuf_empty");
	check_reset_locks(SOME_LOCKS);

	success = true;
	for (uint8_t i = 0; i < size && success; i++)
	{
		reset_locks();
		success &= xringbuf_push_one(&xrb, &i);
		check_reset_locks(2); // One before push, one after

		if (!success)
			diag("failed at: %u", i);
		if (xringbuf_empty(&xrb))
		{
			diag("xringbuf_empty failed at: %u", i);
			success = false;
		}
		if (xringbuf_space(&xrb) != size - i - 1)
		{
			diag("xringbuf_space failed at: %u", i);
			success = false;
		}
		if (xringbuf_content(&xrb) != (size_t)(i + 1))
		{
			diag("xringbuf_content failed at: %u", i);
			success = false;
		}
	}
	ok(success, "push a full buffer one at a time");
	ok(!xringbuf_empty(&xrb), "xringbuf_empty after full push");
	ok(!xringbuf_push_one(&xrb, "x"), "push one too many");
	cmp_ok(xringbuf_content(&xrb), "==", size, "xringbuf_content after over-push");
	ok(!xringbuf_empty(&xrb), "xringbuf_empty after over-push");
	check_reset_locks(SOME_LOCKS);

	success = true;
	for (uint8_t i = 0; i < size && success; i++)
	{
		uint8_t out;
		reset_locks();
		success &= xringbuf_pop_one(&xrb, &out);
		check_reset_locks(2); // One before pop, one after
		if (!success)
			diag("failed at: %u", i);
		if (out != i)
		{
			diag("incorrect number: expected %u, got %u",
			     i, out);
			success = false;
		}
	}
	ok(success, "pop the buffer one at a time");
	cmp_ok(xringbuf_content(&xrb), "==", 0, "xringbuf_content after full pop");
	ok(xringbuf_empty(&xrb), "xringbuf_empty after full pop");
	check_reset_locks(SOME_LOCKS);

	ok(!xringbuf_pop_one(&xrb, tempbuf), "pop one too many");
	cmp_ok(xringbuf_content(&xrb), "==", 0, "xringbuf_content after over-pop");
	ok(xringbuf_empty(&xrb), "xringbuf_empty after full pop");
	check_reset_locks(SOME_LOCKS);

	// Should fit in the buffer, but take up more than half.
#define TEST_STR "Hello"
	size = xringbuf_init(&xrb, BUFFER, 8);
	cmp_ok(size, "==", 7, "re-init");
	reset_locks();
	ok(xringbuf_push(&xrb, TEST_STR, sizeof(TEST_STR)), "push a string");
	check_reset_locks(2);
	cmp_ok(xringbuf_content(&xrb), "==", sizeof(TEST_STR), "xringbuf_content after pushing string");
	ok(!xringbuf_push(&xrb, TEST_STR, sizeof(TEST_STR)), "push too much");
	cmp_ok(xringbuf_content(&xrb), "==", sizeof(TEST_STR), "xringbuf_content after atomic failure");
	reset_locks();
	ok(xringbuf_pop(&xrb, tempbuf, sizeof(TEST_STR)), "pop the string exactly");
	check_reset_locks(2);
	is(tempbuf, TEST_STR, "popped string matches");
	cmp_ok(xringbuf_content(&xrb), "==", 0, "xringbuf_content after exact string pop");

	memset(tempbuf, 0, sizeof(tempbuf));
	ok(xringbuf_push(&xrb, TEST_STR, sizeof(TEST_STR)), "push the string again");
	cmp_ok(xringbuf_content(&xrb), "==", sizeof(TEST_STR), "xringbuf_content after second push");
	check_reset_locks(NO_LOCK_COUNT);
	count = xringbuf_pop_some(&xrb, tempbuf, sizeof(tempbuf));
	check_reset_locks(2);
	cmp_ok(count, "==", sizeof(TEST_STR), "xringbuf_pop_some returns the right amount");
	cmp_ok(xringbuf_content(&xrb), "==", 0, "xringbuf_content xringbuf_pop_some");

	memset(tempbuf, 0, sizeof(tempbuf));
	ok(xringbuf_push(&xrb, TEST_STR, sizeof(TEST_STR)), "push the string again");
	check_reset_locks(SOME_LOCKS);
	count = xringbuf_pop_some(&xrb, tempbuf, 4);
	check_reset_locks(2);
	cmp_ok(count, "==", 4, "partial xringbuf_pop_some, 1/2");
	cmp_mem(tempbuf, TEST_STR, 4, "correct content");
	reset_locks();
	count = xringbuf_pop_some(&xrb, tempbuf, 4);
	check_reset_locks(2);
	cmp_ok(count, "==", 2, "partial xringbuf_pop_some, 2/2");
	is(tempbuf, TEST_STR + 4, "correct content");

	// Shouldn't fit in the buffer
#undef TEST_STR
#define TEST_STR "Hello, world!"
	size = xringbuf_init(&xrb, BUFFER, 8);
	cmp_ok(size, "==", 7, "re-init");
	count = xringbuf_push_some(&xrb, TEST_STR, sizeof(TEST_STR));
	cmp_ok(count, "==", size, "xringbuf_push_some with too much");
	check_reset_locks(SOME_LOCKS);

	memset(tempbuf, 0, sizeof(tempbuf));
	ok(xringbuf_pop(&xrb, tempbuf, count), "pop the partial string");
	cmp_mem(tempbuf, TEST_STR, count, "correct content");
	check_reset_locks(SOME_LOCKS);

	count = xringbuf_push_some(&xrb, TEST_STR, sizeof(TEST_STR));
	assert(count);
	assert(!xringbuf_empty(&xrb));
	xringbuf_clear(&xrb);
	ok(xringbuf_empty(&xrb), "xringbuf_empty after xringbuf_clear");
	check_reset_locks(SOME_LOCKS);

	// Test a bunch of zero-length pushes and pops
	// I'm going to be checking that the xrb state doesn't change, so
	// i'll just grab a copy of the buffer struct after init and compare
	size = xringbuf_init(&xrb, BUFFER, 8);
	xringbuf_t const clean = xrb;
	ok(xringbuf_push(&xrb, "Hello", 0), "zero-length xringbuf_push");
	cmpbuf_ok(&xrb, &clean, "unchanged after xringbuf_push");
	cmp_ok(xringbuf_push_some(&xrb, "Hello", 0), "==", 0, "zero-length xringbuf_push_some");
	cmpbuf_ok(&xrb, &clean, "unchanged after xringbuf_push_some");
	ok(xringbuf_pop(&xrb, tempbuf, 0), "zero-length xringbuf_pop");
	cmpbuf_ok(&xrb, &clean, "unchanged after xringbuf_pop");
	cmp_ok(xringbuf_pop_some(&xrb, tempbuf, 0), "==", 0, "zero-length xringbuf_pop_some");
	cmpbuf_ok(&xrb, &clean, "unchanged after xringbuf_pop_some");
	check_reset_locks(SOME_LOCKS);

	lives_ok(
		{
			ok(xringbuf_push(&xrb, NULL, 0),
			   "xringbuf_push accepts an empty null buffer");
			check_reset_locks(NO_LOCK_COUNT);
		},
		"xringbuf_push doesn't deref a zero-length null buffer"
	);

	lives_ok(
		{
			cmp_ok(xringbuf_push_some(&xrb, NULL, 0), "==", 0,
			   "xringbuf_push_some accepts an empty null buffer");
			check_reset_locks(NO_LOCK_COUNT);
		},
		"xringbuf_push_some doesn't deref a zero-length null buffer"
	);

	lives_ok(
		{
			ok(xringbuf_pop(&xrb, NULL, 0),
			   "xringbuf_pop accepts an empty null buffer");
			check_reset_locks(NO_LOCK_COUNT);
		},
		"xringbuf_pop doesn't deref a zero-length null buffer"
	);

	lives_ok(
		{
			cmp_ok(xringbuf_pop_some(&xrb, NULL, 0), "==", 0,
			   "xringbuf_pop_some accepts an empty null buffer");
			check_reset_locks(NO_LOCK_COUNT);
		},
		"xringbuf_pop_some doesn't deref a zero-length null buffer"
	);

	done_testing();
}
